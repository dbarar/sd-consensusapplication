package main;

import main.algorithms.APP;
import main.algorithms.PL;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.IOException;

public class Main {
    public static Logger logger;

    public static void main(String[] args) throws IOException {
        if (args.length < 5) {
            java.lang.System.err.println("Needing 5 arguments: ProcessIndex hubAddress hubPort processesAddress processPort");
            return;
        }

        int processIndex = Integer.parseInt(args[0]);
        String hubAddress = args[1];
        int hubPort = Integer.parseInt(args[2]);
        String processAddress = args[3];
        int port = Integer.parseInt(args[4]);
        java.lang.System.setProperty("port", String.valueOf(port));

        BasicConfigurator.configure();
        logger = Logger.getLogger(Main.class.getName());
        logger.debug("ProcessIndex: " + processIndex
                + "\n HubAddress: " + hubAddress
                + "\n HubPort: " + hubPort
                + "\n ProcessAddress: " + processAddress
                + "\n MyPort: " + port);

        System system = new System(processIndex, hubAddress, hubPort, processAddress, port);

        APP app = new APP(system);
        system.addAPPtoAlgorithmList(app);

        PL pl = new PL(system);
        system.addPLtoAlgorithmList(pl);

        system.handle();
    }
}
//paxos-windows-386.exe 127.0.0.1 5000 127.0.0.1 5001 5002 5003
