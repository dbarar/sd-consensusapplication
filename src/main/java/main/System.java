package main;

import main.algorithms.*;
import org.javatuples.Pair;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class System {
    private String systemId;
    private int processIndex;
    private String hubAddress;
    private int hubPort;
    private String processAddress;
    private Integer processPort;
    private List<Consensus.ProcessId> processIdList;
    private ConcurrentLinkedQueue<Algorithm> algorithmList;
    private ConcurrentLinkedQueue<Consensus.Message> messageList;
    private Consensus.ProcessId selfProcessId;

    public System(int processIndex,
                  String hubAddress,
                  int hubPort,
                  String processAddress,
                  Integer processPort) {
        this.processIndex = processIndex;
        this.hubAddress = hubAddress;
        this.hubPort = hubPort;
        this.processAddress = processAddress;
        this.processPort = processPort;
        this.algorithmList = new ConcurrentLinkedQueue<>();
        this.messageList = new ConcurrentLinkedQueue<>();
        this.systemId = "sys-1";
    }

    // Event loop
    public void handle() {
        Thread thread = new Thread(() -> {
            while (true) {
                for (Consensus.Message message : messageList) {
                    boolean handled = false;
                    for (Algorithm algorithm : algorithmList) {
                        handled = algorithm.handle(message) || handled;
                        if (handled) {
                            messageList.remove(message);
//                            Main.logger.debug("REMOVE MessageListSize: " + messageList.size() + " " + message.getType());
                        }
                    }
                }
            }
        });
        thread.start();
    }

    public void send(String address, int port, Consensus.Message toSendMessage) {
        try {
            byte[] byteArray = toSendMessage.toByteArray();
            byte[] byteArraySize = ByteBuffer.allocate(4).putInt(byteArray.length).array();

            ByteBuffer byteBufferSize = ByteBuffer.wrap(byteArraySize);
            ByteBuffer byteBufferMsg = ByteBuffer.wrap(byteArray);

            SocketChannel socketChannel = SocketChannel.open();
            socketChannel.connect(new InetSocketAddress(address, port));
            if(socketChannel.isConnected()){
                socketChannel.write(byteBufferSize);
                socketChannel.write(byteBufferMsg);
//                Main.logger.debug("System: sending to: " + socketChannel.getRemoteAddress() + "message: \n" + toSendMessage);
                socketChannel.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addPaxosAlgorithms() {
        algorithmList.add(new EPFD(this));
        algorithmList.add(new ELD(this));
        algorithmList.add(new EC(this));
        algorithmList.add(new UC(this));
        algorithmList.add(new BEB(this));
    }

    public void addEPtoAlgorithmList(Consensus.ProcessId leader, int ts, Pair<Integer, Consensus.Value> state){
        Main.logger.debug("UC: starting new epoch");
        algorithmList.add(new EP(this, leader, ts, state));
    }

    public void addPLtoAlgorithmList(PL pl) {
        algorithmList.add(pl);
        pl.networkListener();
    }

    public void addAPPtoAlgorithmList(APP app) {
        algorithmList.add(app);
        app.run();
    }

    public void addToMessageList(Consensus.Message message) {
        messageList.add(message);
        Main.logger.debug("ADD MessageListSize: " + messageList.size() + " " + message);
    }

    public Integer getProcessPort() {
        return processPort;
    }


    public ConcurrentLinkedQueue<Consensus.Message> getMessageList() {
        return messageList;
    }

    public String getHubAddress() {
        return hubAddress;
    }

    public int getHubPort() {
        return hubPort;
    }

    public String getProcessAddress() {
        return processAddress;
    }

    public int getProcessIndex() {
        return processIndex;
    }

    public List<Consensus.ProcessId> getProcessIdList() {
        return processIdList;
    }

    public void setProcessIdList(List<Consensus.ProcessId> processIdList) {
        this.processIdList = processIdList;
    }

    public String getSystemId() {
        return systemId;
    }

    public Consensus.ProcessId getSelfProcessId() {
        return selfProcessId;
    }

    public void setSelfProcessId(Consensus.ProcessId selfProcessId) {
        this.selfProcessId = selfProcessId;
    }
}