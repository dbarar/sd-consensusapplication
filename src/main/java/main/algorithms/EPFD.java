package main.algorithms;

import main.Consensus;
import main.Main;
import main.System;
import main.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/*
Algorithm 2.7: Increasing Timeout
Implements:
    EventuallyPerfectFailureDetector, instance ✸P.
Uses:
    PerfectPointToPointLinks, instance pl.
upon event ⟨ ✸P, Init ⟩ do
    alive := Π;
    suspected := ∅;
    delay := ∆;
    starttimer(delay);
upon event ⟨ Timeout ⟩ do
    if alive ∩ suspected ̸= ∅ then
        delay := delay + ∆;
    forall p ∈ Π do
        if (p ̸∈ alive) ∧ (p ̸∈ suspected) then
            suspected := suspected ∪ {p};
            trigger ⟨ ✸P, Suspect | p ⟩;
        else if (p ∈ alive) ∧ (p ∈ suspected) then
            suspected := suspected \ {p};
            trigger ⟨ ✸P, Restore | p ⟩;
            trigger ⟨ pl, Send | p, [HEARTBEATREQUEST] ⟩;
    alive := ∅;
    starttimer(delay);
upon event ⟨ pl, Deliver | q, [HEARTBEATREQUEST] ⟩ do
    trigger ⟨ pl, Send | q, [HEARTBEATREPLY] ⟩;
upon event ⟨ pl, Deliver | p, [HEARTBEATREPLY] ⟩ do
    alive := alive ∪ {p};
 */
public class EPFD implements Algorithm {
    final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private final int DELTA = 100;
    private System system;
    final Runnable timeouts = () -> system.addToMessageList(createEpfdTimeoutMessage());
    private List<Consensus.ProcessId> alive;
    private List<Consensus.ProcessId> suspected;
    private int delay;
    private ScheduledFuture<?> timeoutsHandle;

    public EPFD(System system) {
        this.system = system;
        this.initAlive();
        this.suspected = new ArrayList<>();
        this.delay = DELTA;

        startTimer(delay);
    }

    private Consensus.Message createEpfdTimeoutMessage() {
        Consensus.EpfdTimeout epfdTimeout = Consensus.EpfdTimeout.newBuilder().build();
        return Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EPFD_TIMEOUT)
                .setEpfdTimeout(epfdTimeout)
                .setAbstractionId("epfd")
                .setSystemId(system.getSystemId())
                .build();
    }

    private void initAlive() {
        alive = new ArrayList<>();
        alive.addAll(system.getProcessIdList());
    }

    @Override
    public boolean handle(Consensus.Message message) {
        if (message.getType() == Consensus.Message.Type.EPFD_TIMEOUT) {
//            Main.logger.debug("EPFD: epfd_timeout");
//            Main.logger.debug("EPFD: alive when epfd_timeout: " + alive.size() + "\n" + alive);
//            Main.logger.debug("EPFD: suspected when epfd_timeout: " + suspected);
            if (Utils.intersection(suspected, alive)) {
                delay = delay + DELTA;
                Main.logger.debug("EPFD: increased delay: " + delay);
                restartTimer(delay);
            }

            for (Consensus.ProcessId p : system.getProcessIdList()) {
                if (p.getPort() == system.getProcessPort()){
                    Main.logger.debug("EPFD: my port" + p.getPort());
                    continue;
                }
//                Main.logger.debug("EPFD: in for " + p.getPort());
                if (!Utils.contains(p, alive) && !Utils.contains(p, suspected)) {
                    suspected.add(p);
//                    Main.logger.debug("EPFD: suspected: " + p.getPort());
                    triggerEPFS_Suspect(p);
                } else if (Utils.contains(p, alive) && Utils.contains(p, suspected)) {
                    Utils.remove(suspected, p);
//                    Main.logger.debug("EPFD: restored: " + p.getPort());
                    triggerEPFS_Restore(p);
                }

//                Main.logger.debug("EPFD: Heartbeat_Request for: " + p.getPort());
                triggerEPFD_Heartbeat_Request(p);
            }
            alive = new ArrayList<>();
//            Main.logger.debug("EPFD: alive after epfd_timeout: " + alive);
//            Main.logger.debug("EPFD: suspected after epfd_timeout: " + suspected);

            return true;
        } else if (message.getType() == Consensus.Message.Type.PL_DELIVER) {
            if (message.getPlDeliver().getMessage().getType() == Consensus.Message.Type.EPFD_HEARTBEAT_REQUEST) {
//                Main.logger.debug("EPFD: heartbeatRequest (PL_DELIVER) from: " + message.getPlDeliver().getSender().getPort());
                triggerEPFD_Heartbeat_Reply(message.getPlDeliver().getSender(), message.getPlDeliver().getMessage().getMessageUuid());
                return true;
            } else if (message.getPlDeliver().getMessage().getType() == Consensus.Message.Type.EPFD_HEARTBEAT_REPLY) {
//                Main.logger.debug("EPFD: heartbeatReply (PL_DELIVER) from: " + message.getPlDeliver().getSender().getPort());
                alive.add(message.getPlDeliver().getSender());
//                Main.logger.debug("EPFD: alive " + alive);
                return true;
            }
        }

        return false;
    }

    private void startTimer(int delay) {
//        Main.logger.debug("EPFD: Start timeout " + delay + " milliseconds");
        timeoutsHandle = scheduler.schedule(timeouts, delay, TimeUnit.MILLISECONDS);
    }

    private void restartTimer(int delay) {
        Main.logger.debug("EPFD: Increased timeout to " + delay + " milliseconds");
        timeoutsHandle.cancel(true);
        timeoutsHandle = scheduler.schedule(timeouts, delay, TimeUnit.MILLISECONDS);
    }

    private void triggerEPFS_Suspect(Consensus.ProcessId processId) {
        Consensus.EpfdSuspect epfdSuspect = Consensus.EpfdSuspect.newBuilder()
                .setProcess(processId)
                .build();

        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EPFD_SUSPECT)
                .setEpfdSuspect(epfdSuspect)
                .setAbstractionId("epfd")
                .setSystemId(system.getSystemId())
                .build();
        system.addToMessageList(message);
    }

    private void triggerEPFS_Restore(Consensus.ProcessId processId) {
        Consensus.EpfdRestore epfdRestore = Consensus.EpfdRestore.newBuilder()
                .setProcess(processId)
                .build();

        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EPFD_RESTORE)
                .setEpfdRestore(epfdRestore)
                .setAbstractionId("epfd")
                .setSystemId(system.getSystemId())
                .build();
        system.addToMessageList(message);
    }

    private void triggerEPFD_Heartbeat_Request(Consensus.ProcessId processId) {
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EPFD_HEARTBEAT_REQUEST)
                .setAbstractionId("epfd")
                .setMessageUuid(UUID.randomUUID().toString())
                .setSystemId(system.getSystemId())
                .build();

        Consensus.PlSend plSend = Consensus.PlSend.newBuilder()
                .setDestination(processId)
                .setMessage(message)
                .build();

        Consensus.Message toSendMessage = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.PL_SEND)
                .setPlSend(plSend)
                .setAbstractionId("epfd")
                .setSystemId(system.getSystemId())
                .build();
        system.addToMessageList(toSendMessage);
    }

    private void triggerEPFD_Heartbeat_Reply(Consensus.ProcessId processId, String uuid) {
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EPFD_HEARTBEAT_REPLY)
                .setAbstractionId("epfd")
                .setMessageUuid(uuid)
                .setSystemId(system.getSystemId())
                .build();

        Consensus.PlSend plSend = Consensus.PlSend.newBuilder()
                .setDestination(processId)
                .setMessage(message)
                .build();

        Consensus.Message toSendMessage = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.PL_SEND)
                .setPlSend(plSend)
                .setAbstractionId("epfd")
                .setSystemId(system.getSystemId())
                .build();
        system.addToMessageList(toSendMessage);
    }


}
