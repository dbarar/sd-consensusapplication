package main.algorithms;

import main.Consensus;

public interface Algorithm {

    boolean handle(Consensus.Message message);
}
