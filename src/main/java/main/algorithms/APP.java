package main.algorithms;

import main.Consensus;
import main.Main;
import main.System;

import java.util.List;


public class APP implements Algorithm {
    private System system;

    public APP(System system) {
        this.system = system;
    }

    public void run() {
        system.send(system.getHubAddress(), system.getHubPort(), eventAppRegistrationMessage());
    }

    // Send to hub upon process startup: Message(NetworkMessage(Message(AppRegistration)))
    private Consensus.Message eventAppRegistrationMessage() {
        Consensus.AppRegistration appRegistration = Consensus.AppRegistration.newBuilder()
                .setIndex(system.getProcessIndex())
                .setOwner("Denisa").build();

        Consensus.Message appRegistrationMessage = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.APP_REGISTRATION)
                .setAppRegistration(appRegistration)
                .setAbstractionId("app")
                .setMessageUuid("appRegistration")
                .setSystemId(system.getSystemId())
                .build();

        Consensus.NetworkMessage networkMessage = Consensus.NetworkMessage.newBuilder()
                .setMessage(appRegistrationMessage)
                .setSenderHost(system.getProcessAddress())
                .setSenderListeningPort(system.getProcessPort())
                .build();

        return Consensus.Message.newBuilder()
                .setNetworkMessage(networkMessage)
                .setType(Consensus.Message.Type.NETWORK_MESSAGE)
                .build();
    }

    @Override
    public boolean handle(Consensus.Message message) {
        // Send to all
        if (message.getType().equals(Consensus.Message.Type.APP_PROPOSE)) {
            Main.logger.debug("APP_PROPOSE" + message);

            Consensus.AppPropose appPropose = message.getAppPropose();
            eventAppPropose(appPropose.getValue(), appPropose.getProcessesList());
            return true;
        }

        // Send decided value to the hub
        if (message.getType().equals(Consensus.Message.Type.UC_DECIDE)) {
            Main.logger.debug("APP_DECIDE" + message);
            Consensus.UcDecide ucDecide = message.getUcDecide();
            eventUcDecide(ucDecide.getValue());
            return true;
        }

        return false;
    }

    private void eventAppPropose(Consensus.Value value, List<Consensus.ProcessId> processIdList) {
        system.setProcessIdList(processIdList);
        for (Consensus.ProcessId p: processIdList){
            if(p.getPort() == system.getProcessPort()){
                system.setSelfProcessId(p);
                Main.logger.debug("APP: setSelfProcessId" + p);
            }
        }
        system.addPaxosAlgorithms();

        Consensus.UcPropose ucPropose = Consensus.UcPropose.newBuilder()
                .setValue(value)
                .build();

        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.UC_PROPOSE)
                .setAbstractionId("uc")
                .setSystemId(system.getSystemId())
                .setUcPropose(ucPropose)
                .build();

        system.addToMessageList(message);
    }

    private void eventUcDecide(Consensus.Value value) {
        Consensus.AppDecide appDecide = Consensus.AppDecide.newBuilder()
                .setValue(value)
                .build();

        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.APP_DECIDE)
                .setAppDecide(appDecide)
                .setAbstractionId("app")
                .setSystemId(system.getSystemId())
                .build();

        Consensus.NetworkMessage networkMessage = Consensus.NetworkMessage.newBuilder()
                .setMessage(message)
                .setSenderHost(system.getProcessAddress())
                .setSenderListeningPort(system.getProcessPort())
                .build();

        Consensus.Message ucDecideMessage = Consensus.Message.newBuilder()
                .setNetworkMessage(networkMessage)
                .setType(Consensus.Message.Type.NETWORK_MESSAGE)
                .setSystemId(system.getSystemId())
                .setAbstractionId("app")
                .build();

        system.send(system.getHubAddress(), system.getHubPort(), ucDecideMessage);
    }
}
