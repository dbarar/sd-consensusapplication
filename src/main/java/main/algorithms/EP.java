package main.algorithms;

import main.Consensus;
import main.Main;
import main.System;
import org.javatuples.Pair;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Algorithm 5.6: Read/Write Epoch Consensus
 * Implements:
 * EpochConsensus, instance ep, with timestamp ets and leader ℓ.
 * Uses:
 * PerfectPointToPointLinks, instance pl;
 * BestEffortBroadcast, instance beb.
 * upon event ⟨ ep, Init | state ⟩ do
 * (valts, val) := state;
 * tmpval := ⊥;
 * states := [⊥]
 * N ;
 * accepted := 0;
 * upon event ⟨ ep, Propose | v ⟩ do // only leader ℓ
 * tmpval := v;
 * trigger ⟨ beb, Broadcast | [READ] ⟩;
 * upon event ⟨ beb, Deliver | ℓ, [READ] ⟩ do
 * trigger ⟨ pl, Send | ℓ, [STATE, valts, val] ⟩;
 * upon event ⟨ pl, Deliver | q, [STATE, ts, v] ⟩ do // only leader ℓ
 * states[q] := (ts, v);
 * upon #(states) > N/2 do // only leader ℓ
 * (ts, v) := highest(states);
 * if v ̸= ⊥ then
 * tmpval := v;
 * states := [⊥]
 * N ;
 * trigger ⟨ beb, Broadcast | [WRITE, tmpval] ⟩;
 * upon event ⟨ beb, Deliver | ℓ, [WRITE, v] ⟩ do
 * (valts, val) := (ets, v);
 * trigger ⟨ pl, Send | ℓ, [ACCEPT] ⟩;
 * upon event ⟨ pl, Deliver | q, [ACCEPT] ⟩ do // only leader ℓ
 * accepted := accepted + 1;
 * upon accepted > N/2 do // only leader ℓ
 * accepted := 0;
 * trigger ⟨ beb, Broadcast | [DECIDED, tmpval] ⟩;
 * upon event ⟨ beb, Deliver | ℓ, [DECIDED, v] ⟩ do
 * trigger ⟨ ep, Decide | v ⟩;
 * upon event ⟨ ep, Abort ⟩ do
 * trigger ⟨ ep, Aborted | (valts, val) ⟩;
 * halt; // stop operating when aborted
 */
public class EP implements Algorithm {

    private System system;
    private int N;
    private int ets;
    private Pair<Integer, Consensus.Value> state;
    private Consensus.Value tmpVal;
    private Map<Integer, Pair<Integer, Consensus.Value>> states;
    private int accepted;
    private boolean halted;
    private Consensus.ProcessId leader;

    public EP(System system, Consensus.ProcessId leader, int ts, Pair<Integer, Consensus.Value> state) {
        this.system = system;
        this.N = system.getProcessIdList().size();
        this.ets = ts;
        this.state = state;
        this.accepted = 0;
        this.tmpVal = Consensus.Value.newBuilder()
                .setDefined(false)
                .build();
        this.halted = false;
        this.states = new HashMap<>();
        this.leader = leader;
    }

    @Override
    public boolean handle(Consensus.Message message) {
        if (halted) {
            return false;
        }

//        upon event ⟨ ep, Propose | v ⟩ do // only leader ℓ
//          tmpval := v;
//          trigger ⟨ beb, Broadcast | [READ] ⟩;
        if (message.getType() == Consensus.Message.Type.EP_PROPOSE) {
            Main.logger.debug("EP: EP_PROPOSE");
            tmpVal = message.getEpPropose().getValue();
            triggerBEBBroadcastRead();

            return true;
        } else if (message.getType() == Consensus.Message.Type.BEB_DELIVER) {
            Main.logger.debug("EP: BEB_DELIVER");
//            upon event ⟨ beb, Deliver | ℓ, [READ] ⟩ do
//                trigger ⟨ pl, Send | ℓ, [STATE, valts, val] ⟩;
            if (message.getBebDeliver().getMessage().getType() == Consensus.Message.Type.EP_READ_) {
                Main.logger.debug("EP: EP_READ_");
                triggerPLSendEpState(message.getBebDeliver().getSender());

                return true;
            } else
//                upon event ⟨ beb, Deliver | ℓ, [WRITE, v] ⟩ do
//                  (valts, val) := (ets, v);
//                  trigger ⟨ pl, Send | ℓ, [ACCEPT] ⟩;
                if (message.getBebDeliver().getMessage().getType() == Consensus.Message.Type.EP_WRITE_) {
                    Main.logger.debug("EP: EP_WRITE_");
                    state = new Pair<>(ets, message.getBebDeliver().getMessage().getEpWrite().getValue());
                    triggerPLSendEpAccept(message.getBebDeliver().getSender());

                    return true;
                } else
//                upon event ⟨ beb, Deliver | ℓ, [DECIDED, v] ⟩ do
//                  trigger ⟨ ep, Decide | v ⟩;
                    if (message.getBebDeliver().getMessage().getType() == Consensus.Message.Type.EP_DECIDED_) {
                        Main.logger.debug("EP: EP_DECIDED_");
                        triggerEPDecide(message.getBebDeliver().getMessage().getEpDecided().getValue());

                        return true;
                    }
        } else if (message.getType() == Consensus.Message.Type.PL_DELIVER
                && message.getPlDeliver().getSender().getPort() == leader.getPort()) {
//            upon event ⟨ pl, Deliver | q, [STATE, ts, v] ⟩ do // only leader ℓ
//                states[q] := (ts, v);
            if (message.getPlDeliver().getMessage().getType() == Consensus.Message.Type.EP_STATE_) {
                Main.logger.debug("EP: EP_STATE_");

                states.put(message.getPlDeliver().getSender().getPort(),
                        new Pair<>(message.getPlDeliver().getMessage().getEpState().getValueTimestamp(),
                                message.getPlDeliver().getMessage().getEpState().getValue()));

                int statesSize = states.keySet().size();
                Main.logger.debug("EP: states size" + statesSize);
                //            upon #(states) > N/2 do // only leader ℓ
                //                (ts, v) := highest(states);
                //            if v ̸= ⊥ then
                //            tmpval := v;
                //            states := [⊥]
                //            N ;
                //            trigger ⟨ beb, Broadcast | [WRITE, tmpval] ⟩;
                if (statesSize > N / 2) {
                    int key = highest(states);
                    if (states.get(key).getValue1().isInitialized()) {
                        tmpVal = states.get(key).getValue1();
                    }

                    states = new HashMap<>();
                    Main.logger.debug("EP: tmpVal" + tmpVal.getV());

                    triggerEpWrite();
                }
                return true;
            } else
//                upon event ⟨ pl, Deliver | q, [ACCEPT] ⟩ do // only leader ℓ
//                  accepted := accepted + 1;
                if (message.getPlDeliver().getMessage().getType() == Consensus.Message.Type.EP_ACCEPT_) {
                    Main.logger.debug("EP: EP_ACCEPT_");
                    accepted = accepted + 1;
//                upon accepted > N/2 do // only leader ℓ
//                    accepted := 0;
//                    trigger ⟨ beb, Broadcast | [DECIDED, tmpval] ⟩;
                    if (accepted > N / 2) {
                        accepted = 0;
                        triggerEpDecided();
                    }
                    return true;
                }
        } else
//            upon event ⟨ ep, Abort ⟩ do
//              trigger ⟨ ep, Aborted | (valts, val) ⟩;
//              halt; // stop operating when aborted
            if (message.getType() == Consensus.Message.Type.EP_ABORT) {
                Main.logger.debug("EP: EP_ABORT");
                halted = true;
                triggerEpAborted();
                return true;
            }

        return false;
    }

    private int highest(Map<Integer, Pair<Integer, Consensus.Value>> states) {
        Map.Entry<Integer, Pair<Integer, Consensus.Value>> maxEntry = Collections.max(states.entrySet(),
                Comparator.comparing((Map.Entry<Integer, Pair<Integer, Consensus.Value>> e) -> e.getValue().getValue0()));

        return maxEntry.getKey();
    }

    private void triggerBEBBroadcastRead() {
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EP_READ_)
                .setAbstractionId("ep")
                .setSystemId(system.getSystemId())
                .build();
        Consensus.BebBroadcast bebBroadcast = Consensus.BebBroadcast.newBuilder()
                .setMessage(message)
                .build();
        Consensus.Message messageToSend = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.BEB_BROADCAST)
                .setBebBroadcast(bebBroadcast)
                .setSystemId(system.getSystemId())
                .setAbstractionId("ep")
                .build();

        system.addToMessageList(messageToSend);
    }

    private void triggerPLSendEpState(Consensus.ProcessId destination) {
        Consensus.EpState_ epState_ = Consensus.EpState_.newBuilder()
                .setValue(state.getValue1())
                .setValueTimestamp(state.getSize())
                .build();
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EP_STATE_)
                .setEpState(epState_)
                .setAbstractionId("ep")
                .setSystemId(system.getSystemId())
                .build();
        Consensus.PlSend plSend = Consensus.PlSend.newBuilder()
                .setMessage(message)
                .setDestination(destination)
                .build();
        Consensus.Message messageToSend = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.PL_SEND)
                .setPlSend(plSend)
                .setSystemId(system.getSystemId())
                .setAbstractionId("ep")
                .build();

        system.addToMessageList(messageToSend);
    }

    private void triggerPLSendEpAccept(Consensus.ProcessId destination) {
        Consensus.EpAccept_ epAccept_ = Consensus.EpAccept_.newBuilder()
                .build();
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EP_ACCEPT_)
                .setEpAccept(epAccept_)
                .setAbstractionId("ep")
                .setSystemId(system.getSystemId())
                .build();
        Consensus.PlSend plSend = Consensus.PlSend.newBuilder()
                .setMessage(message)
                .setDestination(destination)
                .build();
        Consensus.Message messageToSend = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.PL_SEND)
                .setPlSend(plSend)
                .setSystemId(system.getSystemId())
                .setAbstractionId("ep")
                .build();

        system.addToMessageList(messageToSend);
    }

    private void triggerEPDecide(Consensus.Value value) {
        Consensus.EpDecide epDecide = Consensus.EpDecide.newBuilder()
                .setEts(ets)
                .setValue(value)
                .build();
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EP_DECIDE)
                .setEpDecide(epDecide)
                .setSystemId(system.getSystemId())
                .setAbstractionId("ep")
                .build();

        system.addToMessageList(message);
    }

    private void triggerEpWrite() {
        Consensus.EpWrite_ epWrite_ = Consensus.EpWrite_.newBuilder()
                .setValue(tmpVal)
                .build();
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EP_WRITE_)
                .setEpWrite(epWrite_)
                .setAbstractionId("ep")
                .setSystemId(system.getSystemId())
                .build();
        Consensus.BebBroadcast bebBroadcast = Consensus.BebBroadcast.newBuilder()
                .setMessage(message)
                .build();
        Consensus.Message messageToSend = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.BEB_BROADCAST)
                .setBebBroadcast(bebBroadcast)
                .setSystemId(system.getSystemId())
                .setAbstractionId("ep")
                .build();

        system.addToMessageList(messageToSend);
    }

    private void triggerEpDecided() {
        Consensus.EpDecided_ epDecided_ = Consensus.EpDecided_.newBuilder()
                .setValue(tmpVal)
                .build();
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EP_DECIDED_)
                .setEpDecided(epDecided_)
                .setAbstractionId("ep")
                .setSystemId(system.getSystemId())
                .build();
        Consensus.BebBroadcast bebBroadcast = Consensus.BebBroadcast.newBuilder()
                .setMessage(message)
                .build();
        Consensus.Message messageToSend = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.BEB_BROADCAST)
                .setBebBroadcast(bebBroadcast)
                .setSystemId(system.getSystemId())
                .setAbstractionId("ep")
                .build();

        system.addToMessageList(messageToSend);
    }

    private void triggerEpAborted() {
        Consensus.EpAborted epAborted = Consensus.EpAborted.newBuilder()
                .setEts(ets)
                .setValueTimestamp(state.getValue0())
                .setValue(state.getValue1())
                .build();
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EP_ABORTED)
                .setEpAborted(epAborted)
                .setSystemId(system.getSystemId())
                .setAbstractionId("ep")
                .build();

        system.addToMessageList(message);
    }
}
