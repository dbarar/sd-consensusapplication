package main.algorithms;

import main.Consensus;
import main.Main;
import main.System;
/*
Algorithm 5.5: Leader-Based Epoch-Change
Implements:
    EpochChange, instance ec.
Uses:
    PerfectPointToPointLinks, instance pl;
    BestEffortBroadcast, instance beb;
    EventualLeaderDetector, instance Ω.
upon event ⟨ ec, Init ⟩ do
    trusted := ℓ0;
    lastts := 0;
    ts := rank(self);
upon event ⟨ Ω, Trust | p ⟩ do
    trusted := p;
    if p = self then
        ts := ts + N;
        trigger ⟨ beb, Broadcast | [NEWEPOCH, ts] ⟩;
upon event ⟨ beb, Deliver | ℓ, [NEWEPOCH, newts] ⟩ do
    if ℓ = trusted ∧ newts > lastts then
        lastts := newts;
        trigger ⟨ ec, StartEpoch | newts, ℓ ⟩;
    else
        trigger ⟨ pl, Send | ℓ, [NACK] ⟩;
upon event ⟨ pl, Deliver | p, [NACK] ⟩ do
    if trusted = self then
        ts := ts + N;
        trigger ⟨ beb, Broadcast | [NEWEPOCH, ts] ⟩;
 */

public class EC implements Algorithm {

    private System system;
    private Consensus.ProcessId trusted;
    private int lastTs;
    private int ts;
    private int N;

    public EC(System system) {
        this.system = system;
        this.N = system.getProcessIdList().size();
        this.trusted = system.getProcessIdList().get(N - 1);
        this.lastTs = 0;
        this.ts = system.getSelfProcessId().getRank();
    }

    @Override
    public boolean handle(Consensus.Message message) {
        if (message.getType() == Consensus.Message.Type.ELD_TRUST) {
            Main.logger.debug("EC: ELD_TRUST");
            trusted = message.getEldTrust().getProcess();
            if (trusted.getPort() == system.getSelfProcessId().getPort()) {
                ts = ts + N;
                Main.logger.debug("EC: ELD_TRUST - triggerBEBNewEpoch");
                triggerBEBNewEpoch();
            }
            return true;
        } else if (message.getType() == Consensus.Message.Type.BEB_DELIVER &&
                message.getBebDeliver().getMessage().getType() == Consensus.Message.Type.EC_NEW_EPOCH_) {

            int newTs = message.getBebDeliver().getMessage().getEcNewEpoch().getTimestamp();

            if (message.getBebDeliver().getSender().getPort() == trusted.getPort() && newTs > lastTs) {
                lastTs = newTs;
                Main.logger.debug("EC: BEB_DELIVER + EC_NEW_EPOCH sender==trusted - triggerStartEpoch");
                triggerStartEpoch(newTs);
            } else {
                Main.logger.debug("EC: BEB_DELIVER + EC_NEW_EPOCH sender!=trusted - triggerPlSendNack");
                triggerPlSendNack(message.getBebDeliver().getSender());
            }
            return true;
        } else if (message.getType() == Consensus.Message.Type.PL_DELIVER &&
                message.getPlDeliver().getMessage().getType() == Consensus.Message.Type.EC_NACK_) {

            if (trusted.getPort() == system.getSelfProcessId().getPort()) {
                ts = ts + N;
                Main.logger.debug("EC: PL_DELIVER + EC_NACK_ - triggerBEBNewEpoch");
                triggerBEBNewEpoch();
            }
            return true;
        }

        return false;
    }

    private void triggerBEBNewEpoch() {
        Consensus.EcNewEpoch_ ecNewEpoch_ = Consensus.EcNewEpoch_.newBuilder()
                .setTimestamp(ts)
                .build();
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EC_NEW_EPOCH_)
                .setEcNewEpoch(ecNewEpoch_)
                .setAbstractionId("ec")
                .setSystemId(system.getSystemId())
                .build();
        Consensus.BebBroadcast bebBroadcast = Consensus.BebBroadcast.newBuilder()
                .setMessage(message)
                .build();
        Consensus.Message toSendMessage = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.BEB_BROADCAST)
                .setBebBroadcast(bebBroadcast)
                .setAbstractionId("ec")
                .setSystemId(system.getSystemId())
                .build();

        system.addToMessageList(toSendMessage);
    }

    private void triggerStartEpoch(int newTs) {
        Consensus.EcStartEpoch ecStartEpoch = Consensus.EcStartEpoch.newBuilder()
                .setNewTimestamp(newTs)
                .setNewLeader(trusted)
                .build();
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EC_START_EPOCH)
                .setEcStartEpoch(ecStartEpoch)
                .setAbstractionId("ec")
                .setSystemId(system.getSystemId())
                .build();

        system.addToMessageList(message);
    }

    private void triggerPlSendNack(Consensus.ProcessId processId) {
        Consensus.EcNack_ ecNack_ = Consensus.EcNack_.newBuilder()
                .build();
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EC_NACK_)
                .setEcNack(ecNack_)
                .setAbstractionId("ec")
                .setSystemId(system.getSystemId())
                .build();
        Consensus.PlSend plSend = Consensus.PlSend.newBuilder()
                .setDestination(processId)
                .setMessage(message)
                .build();
        Consensus.Message messageToSend = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.PL_SEND)
                .setPlSend(plSend)
                .setAbstractionId("ec")
                .setSystemId(system.getSystemId())
                .build();

        system.addToMessageList(messageToSend);
    }
}
