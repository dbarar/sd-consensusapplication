package main.algorithms;

import main.Consensus;
import main.Main;
import main.System;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class PL implements Algorithm {

    private System system;
    private ServerSocketChannel server;

    public PL(System system) throws IOException {
        this.system = system;
        this.server = ServerSocketChannel.open();
        server.bind(new InetSocketAddress(this.system.getProcessAddress(), this.system.getProcessPort()));
    }

    @Override
    public boolean handle(Consensus.Message message) {
        // a process invokes the PL_SEND to request the sending of a message
        if (message.getType() == Consensus.Message.Type.PL_SEND) {
//            Main.logger.debug("Pl_send received: " + message.getType());
            eventPlSend(message);
            return true;
        }
        // the PL_DELIVER is triggered by the algorithm implementing the abstraction on a destination process.
        // Process p delivers m. ?Receives
        if (message.getType() == Consensus.Message.Type.PL_DELIVER) {
//            Main.logger.debug("Pl_deliver received: " + message.getType());
            eventPlDeliver(message);
            return true;
        }

        return false;
    }

    // handle PL_SEND messages: send this type of messages to processes that have that destination
    private void eventPlSend(Consensus.Message message) {
        String addressDest = message.getPlSend().getDestination().getHost();
        int portDest = message.getPlSend().getDestination().getPort();
        Consensus.Message messageToSend = message.getPlSend().getMessage();

        Consensus.NetworkMessage networkMessage = Consensus.NetworkMessage.newBuilder()
                .setSenderHost(addressDest)
                .setSenderListeningPort(system.getProcessPort())
                .setMessage(messageToSend)
                .build();

        Consensus.Message toSendMessage = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.NETWORK_MESSAGE)
                .setAbstractionId(message.getAbstractionId())
                .setSystemId(message.getSystemId())
                .setNetworkMessage(networkMessage)
                .build();

        system.send(addressDest, portDest, toSendMessage);
    }

    private void eventPlDeliver(Consensus.Message message) {
        if (message == null || message.getSerializedSize() == 0) {
            Main.logger.debug("PlDeliver: message null or empty");
            return;
        }
        if (system.getProcessIdList() == null) {
            Consensus.Message messageToSend = message.getNetworkMessage().getMessage();
            system.addToMessageList(messageToSend);
            return;
        }

        if(message.getNetworkMessage() == null){
            Main.logger.debug("PL: PL_DELIVER networkMessage is null");
            return;
        }

        Consensus.ProcessId processId = null;
        for (Consensus.ProcessId p : system.getProcessIdList()) {
            if (p.getPort() == message.getNetworkMessage().getSenderListeningPort()) {
                processId = p;
                break;
            }
        }
        if(processId == null){
            Main.logger.debug("Message with no senderListeningPort: " + message);
            return;
        }

        Consensus.PlDeliver plDeliver = Consensus.PlDeliver.newBuilder()
                .setMessage(message.getNetworkMessage().getMessage())
                .setSender(processId)
                .build();

        Consensus.Message messageToSend = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.PL_DELIVER)
                .setAbstractionId(message.getNetworkMessage().getMessage().getAbstractionId())
                .setPlDeliver(plDeliver)
                .build();

        system.addToMessageList(messageToSend);
    }

    // Get Messages from the network and save them into the messageList from the System
    public void networkListener() {
        Thread thread = new Thread(() -> {
            Main.logger.debug("PL: network listener started: " + system.getProcessPort());
            while (true) {
                try {
                    SocketChannel client = this.server.accept();
                    if ((client != null) && (client.isOpen())) {
                        int messageSize;
                        do {
                            ByteBuffer bufferMessageSize = ByteBuffer.allocate(4);
                            client.read(bufferMessageSize);
                            messageSize = bufferMessageSize.getInt(0);
                        } while (messageSize == 0);

                        ByteBuffer bufferMessage = ByteBuffer.allocate(messageSize);
                        client.read(bufferMessage);

                        byte[] bytes = bufferMessage.array();
                        Consensus.Message m = Consensus.Message.parseFrom(bytes);
                        Main.logger.debug("PL received message: " + m);

                        eventPlDeliver(m);

                        client.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
}
