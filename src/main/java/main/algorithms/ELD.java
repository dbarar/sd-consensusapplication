package main.algorithms;

import main.Consensus;
import main.Main;
import main.System;
import main.Utils;

import java.util.ArrayList;
import java.util.List;

/*
Algorithm 2.8: Monarchical Eventual Leader Detection
Implements:
    EventualLeaderDetector, instance Ω.
Uses:
    EventuallyPerfectFailureDetector, instance ✸P.
upon event ⟨ Ω, Init ⟩ do
    suspected := ∅;
    leader := ⊥;
upon event ⟨ ✸P, Suspect | p ⟩ do
    suspected := suspected ∪ {p};
upon event ⟨ ✸P, Restore | p ⟩ do
    suspected := suspected \ {p};
upon leader ̸= maxrank(Π \ suspected) do
    leader := maxrank(Π \ suspected);
    trigger ⟨ Ω, Trust | leader ⟩;
*/

public class ELD implements Algorithm {
    private System system;
    private List<Consensus.ProcessId> suspected;
    private Consensus.ProcessId leader;

    public ELD(System system) {
        this.system = system;
        suspected = new ArrayList<>();
        leader = null;

        checkAndSetLeader();
        Main.logger.debug("ELD: Initialized leader " + leader);
    }


    @Override
    public boolean handle(Consensus.Message message) {
        if (message.getType() == Consensus.Message.Type.EPFD_SUSPECT) {
            suspected.add(message.getEpfdSuspect().getProcess());
            Main.logger.debug("ELD: epfd_suspect");
            checkAndSetLeader();
            return true;
        } else if (message.getType() == Consensus.Message.Type.EPFD_RESTORE) {
            Utils.remove(suspected, message.getEpfdRestore().getProcess());
            Main.logger.debug("ELD: epfd_restore");
            checkAndSetLeader();
            return true;
        }
        return false;
    }

    private void checkAndSetLeader() {
        Consensus.ProcessId newLeader = null;
        int maxRank = 0;

        for (Consensus.ProcessId p : system.getProcessIdList()) {
            if (Utils.contains(p, suspected)) {
                continue;
            }
            if (p.getRank() > maxRank) {
                newLeader = p;
                maxRank = p.getRank();
            }
        }
        if (newLeader == null) {
            return;
        }

        if (leader == null || leader.getPort() != newLeader.getPort()) {
            leader = newLeader;

            Consensus.EldTrust eldTrust = Consensus.EldTrust.newBuilder()
                    .setProcess(leader)
                    .build();
            Consensus.Message message = Consensus.Message.newBuilder()
                    .setType(Consensus.Message.Type.ELD_TRUST)
                    .setEldTrust(eldTrust)
                    .setAbstractionId("eld")
                    .setSystemId(system.getSystemId())
                    .build();

            Main.logger.debug("ELD: leader " + leader.getPort());
            system.addToMessageList(message);
        }

    }
}
