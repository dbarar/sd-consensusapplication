package main.algorithms;

import main.Consensus;
import main.Main;
import main.System;
import org.javatuples.Pair;
/*
Algorithm 5.7: Leader-Driven Consensus
Implements:
    UniformConsensus, instance uc.
Uses:
    EpochChange, instance ec;
    EpochConsensus (multiple instances).

upon event ⟨ uc, Init ⟩ do
    val := ⊥;
    proposed := FALSE;
    decided := FALSE;
    Obtain the leader ℓ0 of the initial epoch with timestamp 0 from epoch-change inst. ec;
    Initialize a new instance ep.0 of epoch consensus with timestamp 0,
    leader ℓ0, and state (0, ⊥);
    (ets, ℓ) := (0, ℓ0);
    (newts, newℓ) := (0, ⊥);
upon event ⟨ uc, Propose | v ⟩ do
    val := v;
upon event ⟨ ec, StartEpoch | newts′, newℓ′ ⟩ do
    (newts, newℓ) := (newts′, newℓ′);
    trigger ⟨ ep.ets, Abort ⟩;
upon event ⟨ ep.ts, Aborted | state ⟩ such that ts = ets do
    (ets, ℓ) := (newts, newℓ);
    proposed := FALSE;
    Initialize a new instance ep.ets of epoch consensus with timestamp ets,
        leader ℓ, and state state;
upon ℓ = self ∧ val ̸= ⊥ ∧ proposed = FALSE do
    proposed := TRUE;
    trigger ⟨ ep.ets, Propose | val ⟩;
upon event ⟨ ep.ts, Decide | v ⟩ such that ts = ets do
    if decided = FALSE then
        decided := TRUE;
        trigger ⟨ uc, Decide | v ⟩;
 */

public class UC implements Algorithm {
    private System system;
    private Consensus.Value val;

    private boolean proposed;
    private boolean decided;

    private Pair<Integer, Consensus.ProcessId> etsLeader;
    private Pair<Integer, Consensus.ProcessId> newTsNewLeader;

    public UC(System system) {
        this.system = system;
        this.val = Consensus.Value.newBuilder().setDefined(false).build();
        this.proposed = false;
        this.decided = false;
        Consensus.ProcessId leader = system.getProcessIdList().get(system.getProcessIdList().size() - 1);
        this.etsLeader = new Pair<>(0, leader);
        this.newTsNewLeader = new Pair<>(0, null);
        system.addEPtoAlgorithmList(leader, 0, new Pair<>(0, val));
    }

    public synchronized boolean handle(Consensus.Message message) {
        if (message.getType() == Consensus.Message.Type.UC_PROPOSE) {
            Main.logger.debug("UC: UC_PROPOSE" + val.getV());
            val = message.getUcPropose().getValue();
            return true;
        } else if (message.getType() == Consensus.Message.Type.EC_START_EPOCH) {
            Main.logger.debug("UC: EC_START_EPOCH");
            newTsNewLeader = new Pair<>(message.getEcStartEpoch().getNewTimestamp(),
                    message.getEcStartEpoch().getNewLeader());
            triggerEPAbort();
            return true;
        } else if (message.getType() == Consensus.Message.Type.EP_ABORTED) {
            Main.logger.debug("UC: EP_ABORTED");
            if (etsLeader.getValue0() == message.getEpAborted().getEts()) {
                etsLeader = new Pair<>(newTsNewLeader.getValue0(), newTsNewLeader.getValue1());
                proposed = false;

                system.addEPtoAlgorithmList(etsLeader.getValue1(), etsLeader.getValue0(),
                        new Pair<>(message.getEpAborted().getValueTimestamp(), message.getEpAborted().getValue()));

                if (etsLeader.getValue1().getPort() == system.getSelfProcessId().getPort() && val.getDefined() && !proposed) {
                    proposed = true;
                    Main.logger.debug("UC: trigger EP_PROPOSE");
                    triggerEpPropose();
                }
            }
            return true;
        } else if (message.getType() == Consensus.Message.Type.EP_DECIDE) {
            Main.logger.debug("UC: EP_DECIDE");

            if (message.getEpDecide().getEts() == etsLeader.getValue0()) {
                if (!decided) {
                    Main.logger.debug("UC: trigger UcDecide " + message.getEpDecide().getValue().getV());
                    decided = true;
                    triggerUcDecide(message.getEpDecide().getValue());
                }
            }
            return true;
        }
        return false;
    }

    private void triggerEPAbort() {
        Consensus.EpAbort epAbort = Consensus.EpAbort.newBuilder()
                .build();
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EP_ABORT)
                .setEpAbort(epAbort)
                .setSystemId(system.getSystemId())
                .setAbstractionId("uc")
                .build();

        system.addToMessageList(message);
    }

    private void triggerUcDecide(Consensus.Value value) {
        Consensus.UcDecide ucDecide = Consensus.UcDecide.newBuilder()
                .setValue(value)
                .build();
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.UC_DECIDE)
                .setUcDecide(ucDecide)
                .setSystemId(system.getSystemId())
                .setAbstractionId("uc")
                .build();

        system.addToMessageList(message);
    }

    private void triggerEpPropose() {
        Consensus.EpPropose epPropose = Consensus.EpPropose.newBuilder()
                .setValue(val)
                .build();

        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.EP_PROPOSE)
                .setEpPropose(epPropose)
                .setSystemId(system.getSystemId())
                .setAbstractionId("uc")
                .build();

        system.addToMessageList(message);
    }

}
