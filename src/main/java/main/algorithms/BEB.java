/*
Algorithm 3.1: Basic Broadcast
Implements:
    BestEffortBroadcast, instance beb.
Uses:
    PerfectPointToPointLinks, instance pl.

upon event ⟨ beb, Broadcast | m ⟩ do
    forall q ∈ Π do
        trigger ⟨ pl, Send | q,m ⟩;

upon event ⟨ pl, Deliver | p, m ⟩ do
    trigger ⟨ beb, Deliver | p, m ⟩;
*/
package main.algorithms;

import main.Consensus;
import main.Main;
import main.System;

public class BEB implements Algorithm {

    private System system;

    public BEB(System system) {
        this.system = system;
    }

    @Override
    public boolean handle(Consensus.Message message) {
        if (message.getType() == Consensus.Message.Type.BEB_BROADCAST) {
            Main.logger.debug("BEB: Broadcast");
            for (Consensus.ProcessId p : system.getProcessIdList()) {
                triggerBebBroadcast(p, message.getBebBroadcast().getMessage());
            }
            return true;
        } else if (message.getType() == Consensus.Message.Type.PL_DELIVER) {
            Main.logger.debug("BEB: PL_DELIVER " + message.getPlDeliver().getMessage().getType());

            if (message.getPlDeliver().getMessage().getType() == Consensus.Message.Type.NETWORK_MESSAGE) {
                return false;
            }
            if (message.getPlDeliver().getMessage().getType() == Consensus.Message.Type.EPFD_HEARTBEAT_REQUEST) {
                return false;
            }
            if (message.getPlDeliver().getMessage().getType() == Consensus.Message.Type.EPFD_HEARTBEAT_REPLY) {
                return false;
            }

            triggerPlDeliver(message.getPlDeliver().getSender(), message.getPlDeliver().getMessage());
            return true;
        }

        return false;
    }

    private void triggerBebBroadcast(Consensus.ProcessId processId, Consensus.Message bebMessage) {
        Consensus.PlSend plSend = Consensus.PlSend.newBuilder()
                .setMessage(bebMessage)
                .setDestination(processId)
                .build();
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.PL_SEND)
                .setPlSend(plSend)
                .setAbstractionId("beb")
                .setSystemId(system.getSystemId())
                .build();

        system.addToMessageList(message);
    }

    private void triggerPlDeliver(Consensus.ProcessId processId, Consensus.Message deliverMessage) {
        Consensus.BebDeliver bebDeliver = Consensus.BebDeliver.newBuilder()
                .setMessage(deliverMessage)
                .setSender(processId)
                .build();
        Consensus.Message message = Consensus.Message.newBuilder()
                .setType(Consensus.Message.Type.BEB_DELIVER)
                .setBebDeliver(bebDeliver)
                .setAbstractionId("beb")
                .setSystemId(system.getSystemId())
                .build();

        system.addToMessageList(message);
    }
}
