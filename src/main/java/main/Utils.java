package main;

import java.util.List;

public class Utils {

    public static boolean intersection(List<Consensus.ProcessId> list1,
                                       List<Consensus.ProcessId> list2) {
        for (Consensus.ProcessId p : list1) {
            if (contains(p, list2)) {
                return true;
            }
        }
        return false;
    }

    public static boolean contains(Consensus.ProcessId processId, List<Consensus.ProcessId> list) {
        for (Consensus.ProcessId p : list) {
            if (p.getPort() == processId.getPort())
                return true;
        }
        return false;
    }

    public static void remove(List<Consensus.ProcessId> list, Consensus.ProcessId processId) {
        Consensus.ProcessId toDelete = null;
        for (Consensus.ProcessId p : list) {
            if (p.getPort() == processId.getPort())
                toDelete = p;
        }

        if (toDelete != null) {
            list.remove(toDelete);
        }
    }
}
