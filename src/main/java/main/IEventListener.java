package main;

@FunctionalInterface
public interface IEventListener<T> {
    void handle(T obj);
}
